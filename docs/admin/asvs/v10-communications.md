---
title: V10 Communication
metaTitle: V10 Communication | Psono Documentation
meta:
  - name: description
    content: Communications security verification requirements
---

# V10 Communication

Communications security verification requirements

[[toc]]

## ASVS Verification Requirement

| ID                          | Detailed Verification Requirement                                                                                                                                                                                                                                                                                                                              | Level 1                 | Level 2    | Level 3     | Since     |
|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|------------|-------------|-----------|
| 10.1                        | Verify that a path can be built from a trusted CA to each Transport Layer Security (TLS) server certificate, and that each server certificate is valid.                                                                                                                                                                                                        | x                       | x          | x           | 1.0       |
| 10.3                        | Verify that TLS is used for all connections (including both external and backend connections) that are authenticated or that involve sensitive data or functions, and does not fall back to insecure or unencrypted protocols. Ensure the strongest alternative is the preferred algorithm.                                                                    | x                       | x          | x           | 3.0       |
| 10.4                        | Verify that backend TLS connection failures are logged.                                                                                                                                                                                                                                                                                                        |                         |            | x           | 1.0       |
| 10.5                        | Verify that certificate paths are built and verified for all client certificates using configured trust anchors and revocation information.                                                                                                                                                                                                                    |                         |            | x           | 1.0       |
| 10.6                        | Verify that all connections to external systems that involve sensitive information or functions are authenticated.                                                                                                                                                                                                                                             |                         | x          | x           | 1.0       |
| 10.8                        | Verify that there is a single standard TLS implementation that is used by the application that is configured to operate in an approved mode of operation.                                                                                                                                                                                                      |                         |            | x           | 1.0       |
| 10.10                       | Verify that TLS certificate public key pinning (HPKP) is implemented with production and backup public keys. For more information, please see the references below.                                                                                                                                                                                            |                         | x          | x           | 3.0.1     |
| 10.11                       | Verify that HTTP Strict Transport Security headers are included on all requests and for all subdomains, such as Strict-Transport-Security: max-age=15724800; includeSubdomains                                                                                                                                                                                 | x                       | x          | x           | 3.0       |
| 10.12                       | Verify that production website URL has been submitted to preloaded list of Strict Transport Security domains maintained by web browser vendors. Please see the references below.                                                                                                                                                                               |                         |            | x           | 3.0       |
| 10.13                       | Ensure forward secrecy ciphers are in use to mitigate passive attackers recording traffic.                                                                                                                                                                                                                                                                     | x                       | x          | x           | 3.0       |
| 10.14                       | Verify that proper certification revocation, such as Online Certificate Status Protocol (OSCP) Stapling, is enabled and configured.                                                                                                                                                                                                                            | x                       | x          | x           | 3.0       |
| 10.15                       | Verify that only strong algorithms, ciphers, and protocols are used, through all the certificate hierarchy, including root and intermediary certificates of your selected certifying authority.                                                                                                                                                                | x                       | x          | x           | 3.0       |
| 10.16                       | Verify that the TLS settings are in line with current leading practice, particularly as common configurations, ciphers, and algorithms become insecure.                                                                                                                                                                                                        | x                       | x          | x           | 3.0       |




### 10.1

Psono.pw is hosted behind cloudflare which is using proper certificates. The way from Cloudflare to psono is also
protected with a letsencrypt certificate. Psono itself is also validating all certificates of third pary services.

### 10.3

All connections use TLS if possible. The used libraries and their behaviour is not easy to audit, yet only trustworthy
and well known libraries are used and vulnerabily checks are in place that would notify if a security vulnerability like
a possible downgrade attack would be discovered.

### 10.4

In general all failures are logged.

### 10.5

All current clients run in the browser, e.g. Chrome. One can assume that they are verifying the paths and revocation
information proper.

### 10.6

All connections to external systems are proper authenticated

### 10.8 (violation)

There are too many different libraries used, that may fall back to different TLS implementations. I would need
asssitance in order to check each of them, how they operate and what TLS implementation they use under the hood.

### 10.10 (violation)

There is no public key pinning of TLS certificates implemented as it can do more harm than good. Clients pin the public
keys of servers when they connect the first time. Authentication between Psono modules (e.g. server and fileserver) are
also protected by pinned keys.

### 10.11

HSTS is enabled for psono.pw. Default configurations e.g. for nginx have this commented out, with the hint that one
should know what it means in order not to destroy anything. a default max-axe with includeSUbdomains is included there
and active on psono.pw

### 10.12

Psono.pw has been submitted and has already been activated on the HSTS preload of web browser vendors.

### 10.13

Cloudflare is using a configuration that prefers ECDHE in order to support forward secrecy. The same is true for the
default nginx configuration provided in the isntallation guide and the nginx configuration that is active on the
internal nginx server behind psono.pw

### 10.14

OSCP is in place and part of the default nginx configuration provided in the installation guide

### 10.15

I havent audited the Trustchain of letsencrypt certificates nor cloudflare yet I assume that their reputation would
prevent issues here.

### 10.16

I currently assume that cloudflare is doing regular audits / checks of their TLS configration in order to stay up to
date of common best practices. Sporadic checks are in addition performed on the TLS connections behind cloudflare
through tools like https://observatory.mozilla.org/ and https://www.ssllabs.com/ssltest/
