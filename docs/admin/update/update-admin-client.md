---
title: 3. Update Admin Client
metaTitle: Update Psono Admin Client | Psono Documentation
meta:
  - name: description
    content: Update guide of the Psono Admin Client
---

# Update Psono Admin Client

Update guide of the Psono Admin Client

## Update with Docker

1.  Update the docker image

	    docker pull psono/psono-admin-client:latest

2.  Stop old psono-admin-client

        docker stop psono-admin-client

3.  Start new psono-admin-client

        docker run --name psono-admin-client-new \
            -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/portal/config.json \
            -d --restart=unless-stopped -p 10102:80 psono/psono-admin-client:latest

4.  Cleanup

	If everything works you can cleanup your containers with the following commands:

		docker rm psono-admin-client
		docker rename psono-admin-client-new psono-admin-client

If anything fails you should be able to start the old docker container again.

## Update without Docker

The webclient is a pure html / js website, that can be hosted with any webserver and has zero dependencies.

1.  Download the webclient artifact

	Visit the following url and download the webclient:

	[http://psono.jfrog.io/psono/psono/admin-client/latest/webclient.zip](http://psono.jfrog.io/psono/psono/admin-client/latest/webclient.zip)

2.  Backup htdocs/portal folder

	Before you replace any files you should backup your htdocs/portal folder

3.  Install webclient

    Unpack the webclient into the htdocs/portal folder of your webserver.

4.  Restore `config.json`

	Copy the old `config.json` and replace the one in the htdocs/portal folder

If anything fails you should be able to restore your admin webclient restoring the files from the backuped folder.
