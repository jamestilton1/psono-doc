---
title: Setup DO as file repository
metaTitle: Setup Digital Ocean Spaces as file repository | Psono Documentation
meta:
  - name: description
    content: Instructions how to setup a Digital Ocean Spaces bucket as file repository
---

# Setup Digital Ocean Spaces S3 as file repository

Instructions how to setup a Digital Ocean Spaces bucket as file repository



## Pre-requirements

You need to have a Digital Ocean account. If not, you can register here [cloud.digitalocean.com](https://cloud.digitalocean.com/registrations/new)


## Setup Guide

This guide will walk you through the creation of a bucket, the configuration of the bucket and the creation of a
service account, before it helps you to configure it in psono.

### Create bucket

1) Login to cloud.digitalocean.com

2) At the top right click "Create Spaces"

![Step 2 Click Create Spaces](/images/user/file_repository_setup_do/01-create-space.jpg)

3) Specify space information and click "Create a Space"

![Step 3 Specify bucket information](/images/user/file_repository_setup_do/02-configure-space.jpg)

::: tip
Remember the name of the space. You will need it later.
:::


### Configure CORS

1) Click on your bucket and go to "Settings"

![Step 1 Go to settings](/images/user/file_repository_setup_do/01-go-to-settings.jpg)

::: tip
Remember the region of the space. It is part of the url at the top. In the screenshot it is 'fra1'. You will need it later.
:::

2) In "CORS Configurations" click "Add"

![Step 2 Add CORS configurations](/images/user/file_repository_setup_do/02-configure-cors.jpg)

and adjust the setting as shown in the screenshot

3) Click "Save Options"


### Create access key

1) Go to API and click "Generate New Key"

![Step 1 Go to IAM](/images/user/file_repository_setup_do/01-create-spaces-access-key.jpg)

::: tip
Copy the shown Key and Secret. You will need them later.
:::

### Configure the file repository


1) Login to Psono

![Step 13 Login to Psono](/images/user/file_repository_setup_gcs/step13-login-to-psono.jpg)

2) Go to "Other"

![Step 14 Go to other](/images/user/file_repository_setup_gcs/step14-go-to-other.jpg)

3) Go to "File Repositories" and click "Create new file repository"

![Step 15 Go to "File Repositories" and click "Create new file repository"](/images/user/file_repository_setup_gcs/step15-select-file-repository-and-click-create-new-file-repository.jpg)

4) Configure the file repository

Use any descriptive title, select Digital Ocean Spaces as type, add your bucket's name, access key and secret.

![Step 16 Configure the file repository](/images/user/file_repository_setup_do/16-create-file-repository.jpg)

You can now upload files from the datastore to this file repository.


