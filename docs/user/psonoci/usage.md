---
title: Usage Psonoci
metaTitle: Usage Psonoci | Psono Documentation
meta:
  - name: description
    content: How to use Psonoci in your build pipeline
---

# Usage Psonoci (Beta)

The basic command looks like this:

``` bash
psonoci [OPTIONS] <SUBCOMMAND> [FLAGS]
```

## General `OPTIONS`

`OPTIONS` can either be specified in the command or with environment variables and need to be set in all subcommands
(with the exception of `help` of course). All commands accept the following `OPTIONS`.

| Option | Description | Environment Variable | Required |
|--------|-------------|----------------------|----------|
| --api-key-id | api key as uuid | PSONO_CI_API_KEY_ID | yes |
| --api-secret-key-hex | api secret key as 64 byte hex string | PSONO_CI_API_SECRET_KEY_HEX | yes |
| --server-url | Url of the psono backend server | PSONO_CI_SERVER_URL | yes |
| --timeout | Connection timeout in seconds  [default: 60] | PSONO_CI_TIMEOUT | no |


## General `FLAGS`

All commands accept the following `FLAGS`

| Option | Description |
|--------|-------------|
| -h, --help | Prints help information |
| -V, --version | Prints version information |


## Overview Subcommands

Psonoci accepts currently the following subcommands.

| Option | Description |
|--------|-------------|
| help | Prints the help for a specific command |
| secret get | Used to retrieve a specific secret, identified by its ID. |

### Example

``` bash
psonoci help
psonoci secret get --help
```




## Subcommand `help`
Used to print the help for a specific command. It can also be passed in as a `FLAG` with `--help`

### Example

``` bash
psonoci help
```



## Subcommand `secret get`

Used to retrieve a specific secret, identified by its ID.

### Arguments:

| Option | Description |
|--------|-------------|
| secret-id | The secret's uuid, e.g. `25070c66-8950-4264-9b39-11e6d83312e3` |
| secret-value | Which secret value to return (`json` returns all values in a json object). Possible values: `json`, `notes`, `password`, `title`, `url`, `url_filter`, `username`, `gpg_key_email`, `gpg_key_name`, `gpg_key_private`, `gpg_key_public` |

### Example with environment variables

``` bash
export PSONO_CI_API_KEY_ID=eaee77c6-169f-4873-9be3-f2613149baa9
export PSONO_CI_API_SECRET_KEY_HEX=c8321d7a8e5b1f5ec3d969ecb5054c7548a1c709ddab2edae2ff7ff028538917
export PSONO_CI_SERVER_URL=https://example.com/server

psonoci secret get 25070c66-8950-4264-9b39-11e6d83312e3 json
```

### Example without environment variables

``` bash
psonoci \
    --api-key-id eaee77c6-169f-4873-9be3-f2613149baa9 \
    --api-secret-key-hex c8321d7a8e5b1f5ec3d969ecb5054c7548a1c709ddab2edae2ff7ff028538917 \
    --server-url https://example.com/server \
    secret get 25070c66-8950-4264-9b39-11e6d83312e3 json
```


